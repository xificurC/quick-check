;;; quick-check.el --- ERT-driven Quickcheck for Emacs Lisp.

;; Author: Peter Nagy
;; Maintainer: Peter Nagy <petern@riseup.net>
;; URL: https://gitlab.com/xificurC/quick-check
;; Version: 0.0.1
;; Keywords: test

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package gives one the ability to create automated tests
;; that check if a function satisfies a given property.
;; For more information on the origin see
;; <http://www.cse.chalmers.se/~rjmh/QuickCheck/>
;; or
;; <https://en.wikipedia.org/wiki/QuickCheck>

;; The main function to use is `quick-check'.
;; It takes a function do prove or disprove and as many generators
;; as the function's arguments.

;;

(defgen ints (int-gen 0 1000000))

(defun split (str chr)
  (s-split (rx-to-string `(: ,chr) t) str))

(defun unsplit (chr strs)
  (s-join (string chr) strs))

(defun split-test (str)
  (cl-loop for char across str
           always (unsplit char (split char str))))

(quickcheck 'split-test strs)

;;; Code:
(defun quick-check (fn &rest generators)
  "Run quickcheck test set on FN with GENERATORS.")

(provide 'quick-check)
;;; quick-check.el ends here
