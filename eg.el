;;; eg.el --- Elisp generators.

;; Author: Peter Nagy
;; Maintainer: Peter Nagy <petern@riseup.net>
;; URL: https://gitlab.com/xificurC/eg
;; Version: 0.0.1
;; Keywords: data-structures

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides an API to work with generators
;; and some default generators that I hope will be useful to you.

;;; Code:

;;; API
(cl-defgeneric eg/next (eg) "Yield next value.")
(cl-defgeneric eg/empty? (eg)
  "Return nil when generator is empty.

Generators may return `\'never' to let the user know it is inifinte.")

;;; Implementations

(cl-defstruct eg/int-r)
(cl-defmethod eg/next ((eg eg/int-r))
  "Yield next random integer."
  (random))
(cl-defmethod eg/empty? ((eg eg/int-r))
  "Return `never' constantly."
  'never)
(defvar eg/int-r (make-eg/int-r)
  "Random integer generator.")

(cl-defstruct eg/int-r+)
(cl-defmethod eg/next ((eg eg/int-r+))
  "Yield next random positive integer."
  (random most-positive-fixnum))
(cl-defmethod eg/empty? ((eg eg/int-r+))
  "Return `never' constantly."
  'never)
(defvar eg/int-r+ (make-eg/int-r+)
  "Random positive integer generator.")

(cl-defstruct eg/from-list data)
(cl-defmethod eg/next ((eg eg/from-list))
  "Yield next list element."
  (prog1
      (car (eg/from-list-data eg))
    (setf (eg/from-list-data eg) (cdr (eg/from-list-data eg)))))
(cl-defmethod eg/empty? ((eg eg/from-list))
  "Return nil if the whole list was traversed."
  (not (eg/from-list-data eg)))

;; start at 0, then 1, then start rising rapidly toward 100 then slow down again.
;; length 10 -> 100 -- log_x 10 = 100
;;                     x ^ 100  = 10
;;                     x        = sqrt_100(10)

;; (cl-defstruct eg/list eg length)
;; (log)

(cl-defstruct eg/list eg length)
(cl-defmethod eg/next ((eg eg/list))
  "Yield next list."
  (prog1
      (cl-loop repeat (eg/list-length eg)
               collect (eg/next eg/list-eg))
    (incf (eg/list-length eg))))

(defun eg/list (eg)
  (make-eg/list eg 0))

(provide 'eg)
;;; eg.el ends here
